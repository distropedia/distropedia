import Layout from "../components/Layout"

export default function Home() {
  return (
      <Layout title="Distropedia - Home" description="A community maintained informational website about linux distributions." keywords="linux,distros">
        <h1 align="center">Distropedia</h1>
      </Layout>
  )
}
