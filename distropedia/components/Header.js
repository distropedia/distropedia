import Image from 'next/image'
import Link from 'next/link'
import { Info } from 'react-feather';

export default function Header() {
  return (
    <header className='bg-gradient-to-r from-red-400 to-pink-500 shadow w-full'>
      <div className='container mx-auto flex flex-wrap flex-col md:flex-row items-center py-3'>

      <Link href="/">
          <a className='font-bold flex md:w-1/5 title-font  items-center md:justify-start mb-4 md:mb-0'>
            <Image src="/logo.png" width={40} height={40} alt="logo"></Image>
            <span className='ml-3 text-xl'>Distropedia</span>
          </a>
      </Link>

        <nav className='flex flex-wrap md:w-45 items-center justify-end text-base space-x-3 md:ml-auto'>
            <a href="/about">
                <Info size={26} />
            </a>
          
            <a href="https://gitlab.com/distropedia/distropedia" target="_blank" rel="noreferrer">
                <svg role="img" className=" h-6 w-6"  viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="M4.845.904c-.435 0-.82.28-.955.692C2.639 5.449 1.246 9.728.07 13.335a1.437 1.437 0 00.522 1.607l11.071 8.045c.2.145.472.144.67-.004l11.073-8.04a1.436 1.436 0 00.522-1.61c-1.285-3.942-2.683-8.256-3.817-11.746a1.004 1.004 0 00-.957-.684.987.987 0 00-.949.69l-2.405 7.408H8.203l-2.41-7.408a.987.987 0 00-.942-.69h-.006zm-.006 1.42l2.173 6.678H2.675zm14.326 0l2.168 6.678h-4.341zm-10.593 7.81h6.862c-1.142 3.52-2.288 7.04-3.434 10.559L8.572 10.135zm-5.514.005h4.321l3.086 9.5zm13.567 0h4.325c-2.467 3.17-4.95 6.328-7.411 9.502 1.028-3.167 2.059-6.334 3.086-9.502zM2.1 10.762l6.977 8.947-7.817-5.682a.305.305 0 01-.112-.341zm19.798 0l.952 2.922a.305.305 0 01-.11.341v.002l-7.82 5.68.026-.035z"/></svg>
            </a>

            <a href="https://element.io/" target="_blank" rel="noreferrer">
                <svg role="img" className=" h-6 w-6" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Element</title><path d="M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0zm-1.314 4.715c3.289 0 5.956 2.66 5.956 5.943 0 .484-.394.877-.879.877s-.879-.393-.879-.877c0-2.313-1.88-4.189-4.198-4.189-.486 0-.879-.393-.879-.877s.392-.877.879-.877zm-5.092 9.504c-.486 0-.879-.394-.879-.877 0-3.283 2.666-5.945 5.956-5.945.485 0 .879.393.879.877s-.394.876-.879.876c-2.319 0-4.198 1.877-4.198 4.191 0 .484-.395.878-.879.878zm7.735 5.067c-3.29 0-5.957-2.662-5.957-5.944 0-.484.394-.878.879-.878s.879.394.879.878c0 2.313 1.88 4.189 4.199 4.189.485 0 .879.393.879.877 0 .486-.394.878-.879.878zm0-2.683c-.485 0-.88-.393-.88-.876 0-.484.395-.878.88-.878 2.318 0 4.199-1.876 4.199-4.19 0-.484.393-.877.879-.877.485 0 .879.393.879.877 0 3.282-2.667 5.944-5.957 5.944z"/></svg>
            </a>
        </nav>
      </div>
    </header>
  )
}

