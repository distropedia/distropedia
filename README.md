<div align="center">

<h1 align="center">Distropedia</h1>
<p align="center">A modern and opensource alternative to distro watch.</p>

</div>

<br/>

Distropedia is the successor of [DistroBoard](https://github.com/Distroboard), which has been declared defunct due to maintenance and scalability issues.

Development on this project will start from January 2022 onwards.

Brought to you by the same core team members of DistroBoard.

<br/>

## Notes for developers

* Running application in development enviorment

    1. Clone this project

        ```
        git clone https://gitlab.com/distropedia/distropedia.git
        ```
    2. Install dependencies

        ```
        cd distropedia/distropedia
        yarn install
        ```
    3. Start the application in development

        ```
        yarn dev
        ```
* Yarn over NPM

    Overall Yarn is more reliable, provides enhanced security, better stability and beats NPM with sheer speed and performance. We suggest all contributers to use yarn instead of NPM for packag management while working with Distropedia.

    Yarn installation :
    ```
    sudo npm i -g yarn
    ```
* NEXT.js telemetry

    Next.js collects various telemetry data by default. Incase you want to disable telemetry use the following

    1. Check wether telemetry is disabled or enabled

        ```
        cd distropedia/distropedia
        yarn next telemetry status
        ```
    2. Disable telemetry if enabled

        ```
        yarn next telemetry disable
        ```

## License

This project has been licensed under the **GNU AGPLv3 license**.

---

Permissions of this strongest copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. When a modified version is used to provide a service over a network, the complete source code of the modified version must be made available.

<div align="center">

| PERMISSIONS    | CONDITIONS                   | LIMITATIONS |
| -------------- | ---------------------------- | ----------- |
| Commercial use | Disclose source              | Liability   |
| Distribution   | License and copyright notice | Warranty    |
| Modification   | Network use is distribution  | -           |
| Patent use     | Same license                 | -           |
| Private use    | State changes                | -           |

</div>

# Contributors
<div>
[CTZxVULKAN](https://gitlab.com/CTZxVULKAN)

[abdellatif-dev](https://gitlab.com/abdellatif-dev)
</div>
